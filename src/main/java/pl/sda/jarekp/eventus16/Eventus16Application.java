package pl.sda.jarekp.eventus16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Eventus16Application {

    public static void main(String[] args) {
        SpringApplication.run(Eventus16Application.class, args);
    }

}
