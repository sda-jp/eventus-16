package pl.sda.jarekp.eventus16.user;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
//@RequiredArgsConstructor
public class RegistrationController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public RegistrationController(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        RegisterForm registerForm = new RegisterForm();
        registerForm.setUsername("alamakota");
        model.addAttribute("registerForm", registerForm);
        return "user/registerForm";
    }

    @PostMapping("/register")
    public String handleRegisterForm(@ModelAttribute @Valid RegisterForm registerForm,
                                     BindingResult bindingResult,
                                     Model model) {
        System.out.println(registerForm);

        if (bindingResult.hasErrors()) {
//            model.addAttribute("registerForm", registerForm);
            return "user/registerForm";
        }

        // TODO: save to database
        String roleUser = "ROLE_USER";
//        Optional<Role> byRoleName = roleRepository.findByRoleName(roleUser);
//        if (byRoleName.isPresent()) {
//
//        }
//        Role role = byRoleName.orElse(new Role());

        Role role = roleRepository.findByRoleName(roleUser)
                //.orElse(roleRepository.save(new Role(roleUser)));
                .orElseGet(() -> roleRepository.save(new Role(roleUser)));

        User user = userRepository.findByUsername(registerForm.getUsername())
                .orElseThrow(() -> new IllegalArgumentException());


        User newUser = new User();
        newUser.setUsername(registerForm.getUsername());
        newUser.setPasswordHash(passwordEncoder.encode(registerForm.getPassword()));
        newUser.addRole(role);
        userRepository.save(newUser);


        return "redirect:/thank-you-for-registering";
    }

    @GetMapping("/thank-you-for-registering")
    public String showRegisterThankYouPage() {
        return "user/registerFormSuccess";
    }
}
