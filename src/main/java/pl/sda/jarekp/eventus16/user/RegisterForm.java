package pl.sda.jarekp.eventus16.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;

@Getter
@Setter
@ToString(exclude = "password")
public class RegisterForm {
//    @NotNull
    @NotEmpty(message = "Pole nie może być puste.")
//    @NotBlank
//    @Pattern(regexp = ".+@.+", message = "Pole musi spełniać .+@.+")
    @Email
    private String username;
    private String password;
}
