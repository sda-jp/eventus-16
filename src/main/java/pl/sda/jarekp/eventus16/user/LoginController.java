package pl.sda.jarekp.eventus16.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login-page")
    public String showLoginForm() {

        return "user/loginForm";
    }
}
