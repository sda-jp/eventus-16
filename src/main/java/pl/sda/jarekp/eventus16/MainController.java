package pl.sda.jarekp.eventus16;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String homePage() {

        return "homepage";
    }

    @GetMapping("/restricted")
    public String restrictedPage(Authentication authentication) {
        authentication.getName();
        return "restrictedPage";
    }
}
